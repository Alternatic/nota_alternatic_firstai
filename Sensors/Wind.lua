local sensorInfo = {
	name = "WindDirection",
	desc = "Return data about actual wind direction.",
	author = "Alternatic",
	date = "2021-07-18",
	license = "MIT",
}

-- the debug updates were taken from treeMarket/example/ExampleDebug

-- get mandatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other mandatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = 0 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind heading
return function()
	local dirX, dirY, dirZ, strength, normDirX, normDirY, normDirZ = Spring.GetWind()
	local wind_direction = Vec3(normDirX, 0, normDirZ) * strength
	
	local unitID = units[1]
	local x, y, z = Spring.GetUnitPosition(unitID)
	local position = Vec3(x, y, z)
	if (Script.LuaUI('exampleDebug_update')) then
		Script.LuaUI.exampleDebug_update(
			unitID, -- key
			{	-- data
				startPos = position, 
				endPos = position + wind_direction * 25
			}
		)
	end

	return wind_direction
end