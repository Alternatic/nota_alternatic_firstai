local sensorInfo = {
	name = "HillsLocation",
	desc = "Return data about hills on the current map.",
	author = "Alternatic",
	date = "2021-07-19",
	license = "notALicense",
}

local EVAL_PERIOD_DEFAULT = math.huge -- calculate just once

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local SpringGetGroundHeight = Spring.GetGroundHeight

-- @description return hill locations
return function()
	local missionInfo = Sensors.core.MissionInfo()
	local hillHeight = missionInfo.areaHeight
	local mapSizeX = Game.mapSizeX
	local mapSizeZ = Game.mapSizeZ

	local foundHills = {}

	-- find the top left corners of all hills on the map
	for x = 1, mapSizeX, 64
	do
		for z = 1, mapSizeZ, 64
		do
			if (SpringGetGroundHeight(x, z) == hillHeight and SpringGetGroundHeight(x - 64, z) ~= hillHeight and SpringGetGroundHeight(x, z - 64) ~= hillHeight)
			then
				-- refine the found hill

				local xLeft = x - 1
				while (SpringGetGroundHeight(xLeft, z) == hillHeight)
				do
					xLeft = xLeft - 1
				end

				local xRight = x + 1
				while (SpringGetGroundHeight(xRight, z) == hillHeight)
				do
					xRight = xRight + 1
				end

				local zTop = z - 1
				while (SpringGetGroundHeight(x, zTop) == hillHeight)
				do
					zTop = zTop - 1
				end

				local zBottom = z + 1
				while (SpringGetGroundHeight(x, zBottom) == hillHeight)
				do
					zBottom = zBottom + 1
				end

				-- save the hill center
				foundHills[#foundHills + 1] = { x = (xLeft + xRight) / 2, z = (zTop + zBottom) / 2}
			end
		end
	end

	return foundHills
end
