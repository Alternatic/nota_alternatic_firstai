local sensorInfo = {
	name = "UnitsToRescue",
	desc = "Return units that can be rescued on the current map.",
	author = "Alternatic",
	date = "2021-07-21",
	license = "notALicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- not cached

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return ids of units that can be rescued
return function(safeArea)
	local unitsToRescue = {}
	local unitsInArea = Spring.GetUnitsInSphere(safeArea.center.x, safeArea.center.y, safeArea.center.z, math.max(Game.mapSizeX, Game.mapSizeZ), Spring.GetMyTeamID())
	
	for i = 1, #unitsInArea
	do
		local id = unitsInArea[i]
		local unitDefID = Spring.GetUnitDefID(id)
		local x, y, z = Spring.GetUnitPosition(id)

		-- only choose units that are not atlases, peepers, radars or windmills and are not in the safe area
		if (safeArea.center:Distance(Vec3(x, y, z)) > safeArea.radius and unitDefID ~= 27 and unitDefID ~= 115 and unitDefID ~= 123 and unitDefID ~= 163)
		then
			unitsToRescue[#unitsToRescue + 1] = id
		end
	end

	return unitsToRescue
end
