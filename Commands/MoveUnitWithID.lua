function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Moves the unit with the given id to the given position",
        parameterDefs = {
            {
                name = "unitID",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            {
                name = "targetPosition",
                variableType = "expression", -- spring vector
                componentType = "editBox",
                defaultValue = "",
            }
        }
    }
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self, units, parameter)
	local unitID = parameter.unitID
	local targetPosition = parameter.targetPosition

	local x, y, z = SpringGetUnitPosition(unitID)
	local position = Vec3(x, y, z)

	if (Spring.GetUnitIsDead(unitID))
	then
		return FAILURE
	end

	if (position:Distance(Vec3(targetPosition[1], y, targetPosition[3])) < 100)
	then
		return SUCCESS
	else
		SpringGiveOrderToUnit(unitID, CMD.MOVE, targetPosition, {})
		return RUNNING
	end
end
