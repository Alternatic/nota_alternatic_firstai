nota_alternatic_firstai 3.0
====

This repository is for the NOTA track of the Labs for MFF CUNI Human-like Artificial Agents course.

* [dependencies](./dependencies.json)

Behaviour trees
---

* sandsail
* ctp2
* ttdr
* startAtlas

Sensors
---

* Wind
* Hills
* GetUnitsToRescue

Unit categories
---

* commander
* peewees
* warriors

Commands
---

* MoveUnitWithID